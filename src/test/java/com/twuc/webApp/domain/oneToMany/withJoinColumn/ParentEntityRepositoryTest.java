package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ParentEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_and_get_parent_and_child_entity() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们已经分别进行了持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且进行持久化。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        ClosureValue<Long> childValue = new ClosureValue<>();
        ClosureValue<Long> parentValue = new ClosureValue<>();
        // <--start--
        // given
        flushAndClear(em -> {
            ChildEntity childEntity = new ChildEntity("child");
            ChildEntity savedChild = childEntityRepository.save(childEntity);
            childValue.setValue(savedChild.getId());
            ParentEntity parentEntity = new ParentEntity("parent");
            ParentEntity savedParent = parentEntityRepository.save(parentEntity);
            parentValue.setValue(savedParent.getId());

        });


        // when
        flushAndClear(em -> {
            ChildEntity savedChild = childEntityRepository.findById(childValue.getValue()).orElseThrow(RuntimeException::new);
            ParentEntity savedParent = parentEntityRepository.findById(parentValue.getValue()).orElseThrow(RuntimeException::new);
            savedParent.addChild(savedChild);
        });

        // assert
        run(em -> {
            ParentEntity parentEntity = parentEntityRepository.findById(parentValue.getValue()).orElseThrow(RuntimeException::new);
            assertNotNull(parentEntity);
            assertEquals(1, parentEntity.getChildEntities().size());
        });
        // --end-->
    }

    @Test
    void should_save_parent_and_child_at_once() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们均未进行持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且直接对 parent 进行持久化时。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start-

        // given
        ChildEntity childEntity = new ChildEntity("child");
        ParentEntity parentEntity = new ParentEntity("parent");
        ClosureValue<Long> parent = new ClosureValue<>();
        flushAndClear(em -> {
            parentEntity.addChild(childEntity);
            ParentEntity savedParent = parentEntityRepository.save(parentEntity);
            parent.setValue(savedParent.getId());
        });

        run(em -> {
            ParentEntity savedEntity = parentEntityRepository.findById(parent.getValue()).orElseThrow(RuntimeException::new);
            assertNotNull(savedEntity);
            assertEquals(1, savedEntity.getChildEntities().size());
        });

        // --end-->
    }

    @Test
    void should_remove_child() {
        // TODO
        //
        // 请书写如下测试：
        //
        // Given 持久化的 parent 和 child。并且 parent 包含 child。
        // When 当使用某种方法从 parent 中移除 child 并进行持久化。
        // Then 再次查询 parent 的时候，parent 的 children 中已经不包含 child，即关联关系取消了。但是
        //   child 记录本身并未被删除。
        //
        // 在写出测试的过程中也需要考虑如何才能够正确的映射 Entity 呢？
        //
        // <--start-
        ClosureValue<Long> childValue = new ClosureValue<>();
        ClosureValue<Long> parentValue = new ClosureValue<>();
        flushAndClear(em -> {
            ChildEntity childEntity = new ChildEntity("child");
            ChildEntity savedEntity = childEntityRepository.save(childEntity);
            childValue.setValue(savedEntity.getId());

            ParentEntity parentEntity = new ParentEntity("parent");
            parentEntity.addChild(childEntity);
            ParentEntity savedParent = parentEntityRepository.save(parentEntity);
            parentValue.setValue(savedParent.getId());

        });

        flushAndClear(em -> {
            ChildEntity childEntity = childEntityRepository.findById(childValue.getValue()).orElseThrow(RuntimeException::new);
            ParentEntity parentEntity = parentEntityRepository.findById(parentValue.getValue()).orElseThrow(RuntimeException::new);
            parentEntity.removeChild(childEntity);
        });

        run(em -> {
            ParentEntity savedEntity = parentEntityRepository.findById(parentValue.getValue()).orElseThrow(RuntimeException::new);
            assertEquals(0, savedEntity.getChildEntities().size());
            ChildEntity saveChild = childEntityRepository.findById(childValue.getValue()).orElseThrow(RuntimeException::new);
            assertNotNull(saveChild);
            assertEquals("child", saveChild.getName());
        });
        // --end->
    }
}
